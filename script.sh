#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} triggerlist.txt"
    exit -1
fi

TRIGGERS=$(cat ${1})

for trigger in ${TRIGGERS}; do
    iLumiCalc.exe --plots --lumitag=OflLumi-13TeV-009 --livetrigger=L1_EM12 --trigger=${trigger} --xml=/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml,/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml  --lar --lartag=LARBadChannelsOflEventVeto-RUN2-UPD4-04 > ${trigger}.log & 
    iLumiCalc.exe --plots --lumitag=OflLumi-13TeV-001 --livetrigger=L1_EM24VHI --trigger=${trigger} --xml=/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml  --lar --lartag=LARBadChannelsOflEventVeto-RUN2-UPD4-04 > ${trigger}.log17 & 
done
wait
